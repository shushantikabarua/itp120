public class TimeTester
{
    public static void main(String[] args)
    {
        Time t1 = new Time(3, 45, 55);
        Time t2 = new Time(1, 20, 11);
        Time t3 = new Time();

        System.out.println(t1.addTime(t2));
    }
}
