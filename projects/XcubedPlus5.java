class XcubedPlus5
{
    public static double XcubedPlus5 (double n) {
        return Math.pow(n, 3) + 5;   
    }
    public static void main(String[] args) {
        double n = Double.parseDouble(args[0]);

        System.out.println(XcubedPlus5(n));
    }
}
