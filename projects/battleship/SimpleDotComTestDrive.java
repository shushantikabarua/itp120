public class SimpleDotComTestDrive
{ 
    public static void main(String[] args)
    {
        SimpleDotCom dot = new SimpleDotCom();
        String userGuess;
        String result;

        userGuess = "2";
        result = dot.checkYourself(userGuess);
        assert result == "miss" : "Expected miss, got " + result;

        userGuess = "3";
        result = dot.checkYourself(userGuess);
        assert result == "hit" : "Expected hit, got " + result;

        userGuess = "4";
        result = dot.checkYourself(userGuess);
        userGuess = "5";
        result = dot.checkYourself(userGuess);
        assert result == "kill" : "Expected kill, got " + result;
    }
}
