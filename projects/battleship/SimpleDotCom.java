public class SimpleDotCom
{ 
    int[] locationCells;
    int numHits = 0;

    public void setLocationCells(int[] locs)
    {
        locationCells = locs;
    }

    public String checkYourself(String stringGuess)
    {
        int intGuess = Integer.parseInt(stringGuess);
        String score = "miss";

        for (int place : locationCells)
        {
            if (place == intGuess)
            {
                numHits++;
                if (numHits == locationCells.length )
                {
                    return "kill";
                }
                score = "hit";
            }
        }
        return score;
    }
}

