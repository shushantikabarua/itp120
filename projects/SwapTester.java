import java.util.Arrays;

class SwapTester
{
    public static int[] converter(String[] convertee)
    {
        int[] output;
        output = new int[2];
        output[0] = Integer.parseInt(convertee[0]);
        output[1] = Integer.parseInt(convertee[1]);
        return output;
    }
    public static int[] swap1(int[] input)
    {
        int[] a = Arrays.copyOf(input, 2);
        a[0] = a[1];
        a[1] = a[0];
        return a;
    }

    public static int[] swap2(int[] input)
    {
        int[] a = Arrays.copyOf(input, 2);
        int t = a[0];
        a[0] = a[1];
        a[1] = t;
        return a;
    }

    public static int[] swap3(int[] input)
    {
        int[] a = Arrays.copyOf(input, 2);
        a[0] = a[0] - a[1];
        a[1] = a[0] + a[1];
        a[0] = a[1] - a[0];
        return a;
    }

    public static void main(String[] args)
    {
        int[] bargs = converter(args);
        if (args.length >= 2)
        {
           System.out.println("Swap1: " + Arrays.toString(swap1(bargs)));
           System.out.println("Swap2: " + Arrays.toString(swap2(bargs)));
           System.out.println("Swap3: " + Arrays.toString(swap3(bargs)));
        }
    }
}
