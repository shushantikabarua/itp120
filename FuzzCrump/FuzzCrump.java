class FuzzCrump
{
    private String realname;
    private int realage;
    private int numGuess;

    public FuzzCrump(String name)
    {
        realname = name;
        realage = (int)(Math.random() * 10 + 10);
    }
    
    public String sayHi()
    {       
        return "My name is " + realname + " and I just want to say, \n that the thing I like most is to play, play, play, play!";
    }

    public String getMyName()
    {
        return realname;
    }

    public int getGuessCount()
    {
        return numGuess;
    }

    public String howOld(int guess)
    {
        numGuess++;
        if (guess > realage)
            return "I'm not that old.";
        if (guess == realage)
            return "You guessed it!";
        return "I'm older than that.";
    }
}
