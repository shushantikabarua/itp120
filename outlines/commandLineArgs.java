class HelloCLI
{ 
    public static void main(String[] args)
    {
        System.out.println("And the command line args are: ");

        if (args.length > 0)
        {
            for (String val:args)
            {
                System.out.println(val);
            }
        }
        else
        {
            System.out.println("Sorry, no CLI args found :-(");
        }
    }
}
