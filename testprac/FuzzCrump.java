class FuzzCrump
{
    private String name;
    private int age;
    private int numGuess = 0;

    public FuzzCrump(String inputName)
    {
        name = inputName;
        age = (int) (Math.random() * 10 + 10);
    }
    
    public String sayHi()
    {
        return "My name is " + name + " and I just want to say,\n that the thing I like most is to play, play, play, play!"; 
    }

    public String getMyName()
    {
        return name;
    }

    public String howOld(int guess)
    {
        numGuess ++;

        if (age > guess)
            return "I'm older than that.";
        if (age < guess)
            return "I'm not that old.";
        return "You guessed it!";
    }

    public int getGuessCount()
    {
        return numGuess;
    }
}
