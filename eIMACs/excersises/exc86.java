class exc86
{
    public static String money( int n )
    {
        return money((double)n);
    }

    public static String money( double d )
    {
        int m = (int)((d * 100) + .5 );
        int dollars = m / 100;
        int cents = m % 100;
        if (cents < 10)
            return ("$" + dollars + ".0" + cents);
        else
            return ("$" + dollars + "." + cents);
    }

    public static String money( String s )
    {
        return money( Double.parseDouble(s));
    }

    public static void main( String[] args )
    {
        System.out.println( money( "98.6" ) );
    }       
}

// Test 1: money( 0 )
// Output 1: $0.00

// Test 2: money( 12.3456 )
// Output 2: $12.35

// Test 3: money( "98.6" )
// Output 3: $98.60
