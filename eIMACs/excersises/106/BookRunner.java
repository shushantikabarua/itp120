public class BookRunner
{
    public static void main(String[] args)
    {
        Book[] library =
        {
            new Book( "David Copperfield", "Charles Dickens" ),
            new Book( "The Jungle Book", "Rudyard Kipling" ),
            new Book( "The Call of the Wild", "Jack London" ),
            new Book( "Frankenstein", "Mary Shelley" ),
            new Book( "Dracula", "Bram Stoker" ),
            new Book( "Anna Karenina", "Leo Tolstoy" )
        };

        Book longestTitle = library[0];
        for (Book book : library)
        {
            if (book.getTitle().length() > longestTitle.getTitle().length())
            {
                longestTitle = book;
            }
        }
        System.out.println(longestTitle.getAuthor());
    }
}

