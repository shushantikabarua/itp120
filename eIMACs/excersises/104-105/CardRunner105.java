public class CardRunner105
{
    public static void main(String[] args)
    {
        Card[] deck = new Card[ 52 ];
        String[] suits = { "spades", "hearts", "diamonds", "clubs"  }; 
        
        for ( int i = 0 ; i < suits.length ; i++ )
        {
            for ( int k = 2 ; k <= 14 ; k++ )
            {
                deck[(13 * i) + (k - 2)] = new Card(suits[i], k);
            }
        }

        for (Card card : deck)
        {
            System.out.println(card.name());
        }
    }
}

