 /**
 * Models a point in a plane
 * 
 * @author Jeff Elkner
 * @version 1.0 2019-10-26
 */
 

public class APPoint
{
    private double myX;
    private double myY;

    public APPoint( double x, double y )
    {
        myX = x;
        myY = y;
    }

    public double getX() { return myX; }
    public double getY() { return myY; }
    public void setX( double x ) { myX = x; }
    public void setY( double y ) { myY = y; }

    public static String printAPPoint( APPoint p )
    {
        return "(" + p.getX() + "," + p.getY() + ")";
    }
}

