class twelvePointOne 
{
    public static String writeDate( int year, int month, int day, int hours, int minutes, int seconds )
    {
        String finalseconds = "", finalminutes = "", date = "";
      
        if (day <= 0)
            date = month + "/" + year + " ";
        else
            date = month + "/" + day + "/" + year + " ";
          
        if (seconds > 60)
            seconds %= 60;
        if (minutes > 60)
            minutes %= 60;
      
        if (seconds < 10)
            finalseconds = ".0" + seconds;
        if (seconds < 0)
            finalseconds = "";
        else
            finalseconds = "." + seconds;
          
        if (minutes < 10)
            finalminutes = "0" + minutes;
        else 
            finalminutes = "" + minutes;
          
        return (date + hours + ":" + finalminutes + finalseconds);
    }

    public static void main(String[] args)
    {     
        System.out.println(writeDate(2005, 6, 1, 16, 3, -1));
    }
}

// Test 1: 2005, 6, 1, 16, 3, -1
// Output 1: 6/1/2005 16:03

// Test 2: 2040, 1, 21, -1, 2, 39
// Output 2: 1/21/2040 
