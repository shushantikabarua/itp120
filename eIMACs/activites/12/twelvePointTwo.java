class twelvePointTwo
{
    public static String writeDate( int year, int month, int day, int hours, int minutes, int seconds )
    {
        String out = month + "/";
        if ( day > 0 )
            out += day + "/";

        out += year;

        if ( hours >= 0 )
        {
            out += " " + hours + ":";
            minutes %= 60;
            if ( minutes < 10 )
                out += "0";

            out += minutes;

            if ( seconds >= 0 )
            {
                out += ".";
                seconds %= 60;
                if ( seconds < 10 )
                    out += "0";

                out += seconds;
            } 
        }

        return out;
  }
    public static String writeDate( int year, int month, int day, int hours ) 
    { 
        return writeDate( year, month, day, hours, 0, -1 ); 
    }

    public static String writeDate(int year, int month, int day)
    {
        return writeDate(year, month, day, -1, 0, -1);
    }

    public static String writeDate(int year, int month)
    {
        return writeDate(year, month, -1, -1, 0, -1);
    }

    public static void main(String[] args)
    {
        // Method 1
        System.out.println( writeDate( 2008, 3, 25, 16 ) );

        // Method 2
        System.out.println( writeDate( 2008, 7, 4 ) );

        // Method 3
        System.out.println( writeDate( 2013, 9 ) );

    }
}

/*  Test 1: 
    Method 1: writeDate( 2008, 3, 25, 16 )  
    Method 2: writeDate( 2008, 7, 4 )
    Method 3: writeDate( 2013, 9 )

    Output 1: 
    3/25/2008 16:00 
    7/4/2008 
    9/2013

    Test 2:
    Method 1: writeDate( 2008, 7, 25, 12 )
    Method 2: writeDate( 2008, 7, 32 )
    Method 3: writeDate( 2013, 13 )

    Output 2: 
    7/25/2008 12:00 
    7/32/2008 
    13/2013

    Test 3:
    Method 1: writeDate(2008, 8, 18, 12) 
    Method 2: writeDate(1999, 12, 31) 
    Method 3: writeDate( 2000, 1 )

    Output 3: 
    8/18/2008 12:00 
    12/31/1999 
    1/2000

    Test 4:
    Method 1: writeDate( 2008, 11, 25, 8 ) 
    Method 2: writeDate( 2011, 4, 28)
    Method 3: writeDate( 2009, 7 )
    
    Output 4: 
    11/25/2008 8:00 
    4/28/2011 
    7/2009
