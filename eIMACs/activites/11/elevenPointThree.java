class elevenPointThree
{
    public static void main(String[] args)
    {
        System.out.println(bothStart(args[0], args[1]));
    }

    public static String bothStart( String a, String b )
    {
        int z;
        int count = 0;

        if (a.length() <= b.length())
            z = a.length();
        else
            z = b.length();

        boolean checker = false;

        for (int i = 0; i < z && !checker; i++ )
        {
            if (a.substring(0, i).equals(b.substring(0, i)))
                count++;
            else
                checker = true;
        }

        return (a.substring(0, count - 1));
    }
}
