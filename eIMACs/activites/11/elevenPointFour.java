class elevenPointFour
{
    public static void main(String[] args)
    {
        String s = args[0];
        int[] h = new int[args.length - 1];
        for (int i = 1; i < args.length; i++)
            h[i - 1] = Integer.parseInt(args[i]);

        String[] shushan = stringHeads(s, h);
        for (String shu : shushan)
            System.out.println(shu);
    }
    public static String[] stringHeads(String s, int[] p)
    {
        String[] finall = new String[p.length];
        for (int i = 0; i < p.length ; i++)
        {
            if (p[i] > s.length())
                p[i] = s.length();
            finall[i] = s.substring(0, p[i]);
        }
        return finall;
    }
}

