class elevenPointTwo
{
    public static void main(String[] args)
    {
        System.out.println(endsWith(args[0], args[1]));
    }

    public static boolean endsWith( String a, String b )
    {
        if (b.length() > a.length())
            return false;
        else if ((a.substring(a.length() - b.length())).equals(b))
            return true;
        else
            return false;
    }

}

// Test 1: "Enterprise-D" "D"
// Output 1: true

// Test 2: "Voyager" "ager"
// Output 2: true

// Test 3: "Deep Space Nine" "nine"
// Output 3: false

// Test 4: "Deep Space Nine" "Deep Space Nineteen"
// Output 4: false

