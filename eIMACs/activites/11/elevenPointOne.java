class elevenPointOne
{
    public static void main(String[] args)
    {
        System.out.println(startsWith(args[0], args[1]));
    }
    public static boolean startsWith( String a, String b )
    {
        if (b.length() > a.length())
            return false;
        else if ((a.substring(0, b.length())).equals(b))
            return true;
        else
            return false;
    } 

}

// Test 1: "radar installation" "rad"
// Output 1: true

// Test 2: "radar installation" "installation"
// Output 2: false

// Test 3: "radar installation" "" 
// Output 3: true

// Test 4: "" "a" 
// Output 4: false

// Test 5: "" ""
// Output 5: true
