class sevenPointTwo
{ 
    public static void main(String[] args)
    {
        int[] a  = new int[args.length];
        for (int i = 0; i < args.length ; i++)
        {
            a[i] = Integer.parseInt(args[i]);
        }

        int checksum;
        int i = 0;
        
        checksum = 0;

        while ( i < a.length )
        {
        checksum += a[i] ;
        i++;
        }
    
    
        checksum %= 10;

        System.out.println("The checksum is " + checksum);
    }
}

