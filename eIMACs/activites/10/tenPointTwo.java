class tenPointTwo
{
    public static void main(String[] args)
    {
        int[] s = new int[args.length];
        for (int i = 0 ; i < args.length ; i++)
        {
            s[i] = Integer.parseInt(args[i]);
        }
        String[] ordinals = new String[s.length];

        for (int i = 0; i <  s.length; i ++)
        {
            ordinals[i] = s[i] + "";
        }

        boolean teens = false;

        for (int i = 0; i < s.length; i ++)
        {
            switch (s[i] % 100)
            {
                case 11 :
                case 12 :
                case 13 :
                    ordinals[i] += "th";
                    teens = true;
                    break;
            }
            
            if (!teens)
            {
                switch (s[i] % 10)
                {
                    case 1 :
                        ordinals[i] += "st";
                        break;
                    case 2 :
                        ordinals[i] += "nd";
                        break;
                    case 3:
                        ordinals[i] += "rd";
                        break;
                    default:
                        ordinals[i] += "th";
                }
            }
        teens = false;
        }

        for (String num : ordinals)
            System.out.println("Ordinal is " + num);
    }
}

// Test: 21 22 973 1000
// Output: 
    // Ordinal is 21st
    // Ordinal is 22nd
    // Ordinal is 973rd
    // Ordinal is 1000th

// Test: 1 2 3 4 5 
// Output:  
    // Ordinal is 1st
    // Ordinal is 2nd
    // Ordinal is 3rd
    // Ordinal is 4th
    // Ordinal is 5th

// Test: 52 11 74 78 27 
// Output: 

// Test: 
// Output: 
