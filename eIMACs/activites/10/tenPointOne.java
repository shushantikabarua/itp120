class tenPointOne
{
    public static void main(String[] args)
    {
    int  t = Integer.parseInt(args[0]);

    String inwords;
    int onesPlaceInt;
    int tensPlaceInt;
    String onesPlaceString;
    String tensPlaceString;

    onesPlaceInt = 0;
    tensPlaceInt = 0;
    onesPlaceString = "";
    tensPlaceString = "";

    String onesPlace[] =
    {
        "",
        "one",
        "two",
        "three",
        "four",
        "five",
        "six",
        "seven",
        "eight",
        "nine"
    };

    String tensPlace[] =
    {
    "",
    "",
    "twenty ",
    "thirty ",
    "fourty ",
    "fifty "
    };

    String teens[] = 
    {
    "ten",
    "eleven",
    "twelve",
    "thirteen",
    "fourteen",
    "fifteen",
    "sixteen",
    "seventeen",
    "eighteen",
    "nineteen",
    };

    onesPlaceInt = t % 10;
    tensPlaceInt = t/10;

    onesPlaceString = onesPlace[onesPlaceInt];
    tensPlaceString = tensPlace[tensPlaceInt];
    inwords = tensPlaceString + onesPlaceString;

    if (tensPlaceInt == 1)
        inwords = teens[onesPlaceInt];
        
    if (t == 0)
        inwords = "zero";

    System.out.println(inwords);
    }
}

