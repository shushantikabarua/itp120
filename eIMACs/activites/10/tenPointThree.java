class tenPointThree
{
    public static void main(String[] args)
    {
    String s = args[0];
    int value;

    s = s.replace("s", "");
    s = s.replace("t", "");
    s = s.replace("r", "");
    s = s.replace("d", "");
    s = s.replace("h", "");
    s = s.replace("n", "");
    
    value = Integer.parseInt(s);
    System.out.println(s);
    }
}

// Test: 1st
// Output: 1

// Test: 200th
// Output: 200

// Test: 22nd
// Output: 22

// Test: 53rd
// Output: 53
