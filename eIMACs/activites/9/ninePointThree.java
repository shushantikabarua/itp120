class ninePointThree
{
    public static void main(String[] args)
    {
        double[] data = new double[args.length];
        for (int i = 0 ; i < args.length ; i++)
        {
            data[i] = Double.parseDouble(args[i]);
        }
        
        double sum;
        double mean;
        double sub;
        double sum2;
        double mean2;
        double sd;

        sum = 0;
        mean2 = 0;
        sum2 = 0;

        for (double i : data)
        {       
            sum += i;
        }

        mean = sum / data.length;
        
        for (double i : data)
        {
            sub = Math.pow((i - mean), 2);
            sum2 += sub;
        }
        
        mean2 = sum2 / data.length;
        sd = Math.sqrt(mean2);

        System.out.println("standard deviation is " + sd);
    }
}
// Test 1: 1.0 2.0 3.0 4.0 5.0
// Output 1: standard deviation is 1.4142136

// Test 2: 0.1 1.2 2.3 3.4 4.5 5.6
// Output 2: standard deviation is 1.8786076

// Test 3: 0.5 100 20.4 100 53.8 100
// Output 3: standard deviation is 40.6424142

// Test 4: 0 1 4 9 16
// Output 4: standard deviation is 5.8991525
