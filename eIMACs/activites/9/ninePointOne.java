class ninePointOne
{ 
    public static void main(String[] args)
    {
        double[] data = new double[args.length];
        for (int i = 0 ; i < args.length ; i++)
        {
            data[i] = Double.parseDouble(args[i]);
        }

        double sum, product;

        sum = 0;
        product = 1;
    
        for (double i : data)
        {
            sum += i;
            product *= i;
        }

        System.out.println("The sum is " + sum);
        System.out.println("The product is " + product);
    }
}

// Test 1: 62.08 24.49 69.0 48.7 96.73 
// Output 1: The sum is 301.0  The product is 4.9417381906500477E8 

// Test 2: 65.32 39.23 -68.55 -21.07 8.76 
// Output 2: The sum is 23.689999999999984  The product is 3.242205850272429E7

// Test 3: 8.92 68.36 37.7 51.56 16.14 
// Output 3: The sum is 182.68  The product is 1.913042849364442E7

// Test 4: 35.69 -48.86 29.87 -61.94 -13.8 
// Output 4: The sum is -59.03999999999999  The product is -4.4523112853563175E7
