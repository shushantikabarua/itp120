class ninePointTwo
{ 
    public static void main(String[] args)
    {
        double[] data = new double[args.length];
        for (int i = 0 ; i < args.length ; i++)
        {
            data[i] = Double.parseDouble(args[i]);
        }
        
        double max,min;

        max = data[0];
        min = data[0];
    
        for ( int i = 1; i < data.length; i ++ )
        {
            if ( data[i] > max)
                max = data[i];
            else if ( data[i] < min )
                min = data[i];
        }

        System.out.println("Max is " + max);
        System.out.println("Min is " + min);
    }
}

// Test 1: 15.53 29.15 22.15 52.66 24.25
// Output 1: Max is 52.66  Min is 15.53

// Test 2: 41.29 41.89 5.77 21.59 56.95
// Output 2: Max is 56.95  Min is 5.77

// Test 3: 47.69 77.02 68.74 68.03 24.24
// Output 3: Max is 77.02  Min is 24.24

// Test 4: 47.34 57.3 35.94 76.86 90.48
// Output 4: Max is 90.48  Min is 35.94.
