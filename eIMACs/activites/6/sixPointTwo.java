class sixPointTwo
{
    public static void main (String[] args)
    {
        System.out.println(mealCode("vegetarian"));
        System.out.println("vegetarian".indexOf("vegetarian"));
    }
    public static int mealCode (String request)
    {
        int mealcode = 0;
        if ((request.indexOf("no beef") >= 0 || request.indexOf("chicken") >= 0) && (request.indexOf("child") == -1 ))
            mealcode = 1;
        else if ((request.indexOf("beef") >= 0 ) && (request.indexOf("child") == -1 ))
            mealcode = 2;
        else if (request.indexOf("vegetarian") >= 0 )
            mealcode = 3;
        else if ((request.indexOf("child") >= 0 ) && (request.indexOf("vegetarian") == -1 ))
            mealcode = 4;
        else
            mealcode = 0;
        return mealcode;
    }
}
