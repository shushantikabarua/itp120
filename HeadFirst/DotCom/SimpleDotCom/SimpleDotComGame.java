public class SimpleDotComGame
{ 
    int[] locationCells = {3, 4, 5};
    int numHits = 0;
    int guessCounter = 0;

    public void setLocationCells(int[] locs)
    {
        locationCells = locs;
    }

    public boolean game.over()
    {
        return numHits > 2;
    }

    public String displayResults()
    {
        System.out.println("You took " + guessCounter + " guesses");
    }

    public String makeMove(String stringGuess)
    {
        int intGuess = Integer.parseInt(stringGuess);
        String score = "miss";

        for (int place : locationCells)
        {
            if (place == intGuess)
            {
                numHits++;
                if (numHits == locationCells.length )
                {
                    guessCounter++;
                    return "kill";
                }
                guessCounter++;
                score = "hit";
            }
        }
        return score;
    }
}

