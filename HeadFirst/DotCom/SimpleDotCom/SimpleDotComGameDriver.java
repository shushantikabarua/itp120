public class SimpleDotComGameDriver
{
    public static void main(String[] args)
    {
        GameHelper helper = new GameHelper();
        SimpleDotComGame game = new SimpleDotComGame();
 
        while (!game.over())
        {
            System.out.println(
                game.makeMove(helper.getUserInput("Enter a number: "))
            );
        }
        System.out.println(game.displayResults());
    }
}
