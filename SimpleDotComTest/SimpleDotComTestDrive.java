public class SimpleDotComTestDrive
{ 
    public static void main(String[] args)
    {
        SimpleDotComGame dot = new SimpleDotComGame();
        String userGuess;
        String result;

        userGuess = "2";
        result = dot.makeMove(userGuess);
        assert result == "miss" : "Expected miss, got " + result;

        userGuess = "3";
        result = dot.makeMove(userGuess);
        assert result == "hit" : "Expected hit, got " + result;

        userGuess = "4";
        result = dot.makeMove(userGuess);
        userGuess = "5";
        result = dot.makeMove(userGuess);
        assert result == "kill" : "Expected kill, got " + result;

    }
}

